#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Agent',
    'name_de_DE': 'Fakturierung Sachbearbeiter',
    'version': '2.2.0',
    'author': 'virtual-things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Account Invoice Agent
    - Adds the field of an agent party to the invoice
    ''',
    'description_de_DE': '''Fakturierung Sachbearbeiter
    - Fügt das Feld Sachbearbeiter zu Rechnungen hinzu
''',
    'depends': [
        'account_invoice',
        'party_rollup_person',
    ],
    'xml': [
        'account_invoice.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
