# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelWorkflow, ModelView, ModelSQL, fields
from trytond.pool import Pool
from trytond.pyson import Equal, Eval, Not, Or


class Invoice(ModelWorkflow, ModelSQL, ModelView):
    _name = 'account.invoice'

    agent = fields.Many2One('party.party', 'Agent',
            domain=[('party_type', '=', 'person'),
                    ('organization_members.organization', 'in', [Eval('party'),])
                ],
            states={
                'readonly': Or(
                    Not(Equal(Eval('state'), 'draft')),
                    Not(Eval('party', False)),
                    Not(Equal(Eval('party_type'), 'organization')),
                    ),
            }, on_change_with=['party'], depends=[
                'party', 'party_type', 'state'])
    party_type = fields.Function(fields.Char('Party Type',
            on_change_with=['party']), 'get_party_type')

    def on_change_with_agent(self, vals):
        if not vals.get('party'):
            return None

    def on_change_with_party_type(self, vals):
        party_obj = Pool().get('party.party')
        if vals.get('party'):
            party = party_obj.browse(vals['party'])
            if party.party_type:
                return party.party_type
        return ''

    def get_party_type(self, ids, name):
        res = {}
        for invoice in self.browse(ids):
            if invoice.party and invoice.party.party_type:
                res[invoice.id] = invoice.party.party_type
            else:
                res[invoice.id] = ''
        return res

    def _credit(self, invoice):
        res = super(Invoice, self)._credit(invoice)
        res['agent'] = invoice.agent.id
        return res

Invoice()
